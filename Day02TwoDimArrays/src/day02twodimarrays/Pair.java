package day02twodimarrays;

public class Pair {
    public ValAtRowCol val1;
    public ValAtRowCol val2;

    public Pair(ValAtRowCol val1, ValAtRowCol val2) {
        this.val1 = val1;
        this.val2 = val2;
    }

    @Override
    public String toString() {
        return String.format("Prime sum %3d of pair value = %5d at [%d,%d] and "
                + "value = %3d at [%d,%d]",val1.val+val2.val,val1.val,val1.row,
                val1.col,val2.val,val2.row,val2.col); 
    }
}

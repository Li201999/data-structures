package day02twodimarrays;

import java.util.ArrayList;
import java.util.Scanner;

public class Day02TwoDimArrays {
    private static boolean isPrime(int num){
        if(num < 1){
            return false;
        }
        if(num == 1){
            return true;
        }
        for(int i = 2; i<=num/2;i++){
            if(num % i == 0){
                return false;
            }
        }
        return true;
    }
    
    //calculate
    //private static int[][]array;
    //1) Sum of all numbers in the array
    private static int totalNum;
    public static int sumOfAllNum(int[][] array){
        totalNum = 0;
        for (int i = 0; i < array.length ; i++) {
            for (int j = 0; j < array[i].length ; j++) {
                totalNum += array[i][j];
            }
        }
        return totalNum;
    }
    
    //2) Sum of each of the row of the array
    public static int[] sumOfEachOfTheRow(int[][] array){
        System.out.print("2) Sum of each of the row of the array : ");
        int[] sumOfEachRow = new int[array.length];
        for (int i = 0; i < array.length ; i++) {
            int eachRowNum = 0;
            for (int j = 0; j < array[i].length ; j++) {
                eachRowNum += array[i][j];
            }
            sumOfEachRow[i] = eachRowNum;
        } 
        return sumOfEachRow;
    }
    
    //3) Sum of each of the column of the array
    public static int[] sumOfEachOfTheColumn(int[][] array){
        System.out.println("");
        System.out.print("3) Sum of each of the column of the array: ");
        int rows = array.length;
        int cols = array[0].length; 
        int[] eachOfColumnNum = new int[cols];  
        for (int j = 0; j < cols ; j++) {
            int eachColsNum = 0;
            for (int i = 0; i < rows ; i++) {
                eachColsNum += array[i][j];
            }
            eachOfColumnNum[j] = eachColsNum;
        }    
        return eachOfColumnNum;
    }
    
    //4) Standard deviation of all numbers in the array
    public static double standardDeviation(int[][] array){
        System.out.println("");
        int rows = array.length;
        int cols = array[0].length; 
        //
        double avgNumber = totalNum / (rows * cols);
        double tempNum = 0;
        //
        for (int i = 0; i < rows ; i++) {
            for (int j = 0; j < cols ; j++) {
                tempNum += Math.pow(array[i][j] - avgNumber, 2);
            }
        }
        double varianceNumber = tempNum / (rows * cols);
        return Math.sqrt(varianceNumber);
    }
    
    //5) Find pairs of numbers in the array whose sum is a prime number 
    //   and display those pairs and their sum.
    public static ArrayList<Pair> pairsOfNumber(int[][] array){ 
        System.out.println("5) Find pairs of numbers in the array whose sum "
                + "is a prime number");
        //get the rows and cols
        int rows = array.length;
        int cols = array[0].length; 
        //pair number(all)
        ValAtRowCol val1; 
        ValAtRowCol val2;
        Pair pair;
        //convert 2-D to 1
        int[] newArray = new int[rows * cols];
        int count = 0;
        for (int i = 0; i < rows ; i++) {
            for (int j = 0; j < cols ; j++) {
                newArray[count] = array[i][j];
                count ++;
            }
        }
        //arrayList set
        // all the array
        ArrayList<Pair> allPairs = new ArrayList<Pair>();
        for (int i = 0; i < newArray.length; i++) {
            for (int j = i + 1; j < newArray.length; j++) {
                val1 = new ValAtRowCol(newArray[i],(int)i/cols,i%cols);
                val2 = new ValAtRowCol(newArray[j],(int)j/cols,j%cols);
                allPairs.add(new Pair(val1,val2));
            }
        }
        //compare loop
        //Prime Array
        System.out.println("display those pairs and their sum: ");
        ArrayList<Pair> primePairs = new ArrayList<Pair>();
        allPairs.forEach(s -> {
           if(isPrime(s.val1.val+s.val2.val)){
               primePairs.add(new Pair(s.val1,s.val2));
               System.out.println(s.toString());
           }
        }); 
        return primePairs;
    }

    //Standard deviation of all numbers in the array
    public static void main(String[] args) {
        //scanner
        Scanner input = new Scanner(System.in);
        int cols = 0,rows = 0;
        try{
            System.out.println("Enter the width of array: ");        
            cols = Integer.parseInt(input.nextLine());
            System.out.println("Enter the height of array: "); 
            rows = Integer.parseInt(input.nextLine());
            if(cols < 1 || rows < 1){
                System.out.println("width number or height number invalid!");
                System.exit(1);
            }
        }catch(NumberFormatException e){
            System.out.println("Number parse invaid! " + e.getMessage());
        
        } 
        int[][] array = new int[rows][cols];
        for (int i = 0; i < rows ; i++) {
            for (int j = 0; j < cols ; j++) {
                array[i][j] = (int) (Math.random() * 199) - 99;
            }
        }
        //Manner print 
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                System.out.printf("%5d  ", array[i][j]);
            }
            System.out.println("");
        }

        //Compute and display:
        //1) Sum of all numbers in the array
        System.out.println("1) Sum of all numbers in the array : " + sumOfAllNum(array));
        //2) Sum of each of the row of the array
        int[] rArray = sumOfEachOfTheRow(array);
        for(int i : rArray){
            System.out.printf("%5d,", i); 
        }
        //3) Sum of each of the column of the array 
        int[] cArray = sumOfEachOfTheColumn(array);
        for(int i : cArray){
            System.out.printf("%5d,", i); 
        }
        //4) Standard deviation of all numbers in the array
        System.out.printf("4) Standard deviation of all numbers "
                        + "in the array: %f ", standardDeviation(array));
        System.out.println("");
        //5) Find pairs of numbers in the array whose sum is a prime number 
        //   and display those pairs and their sum.
        //transfer 2-D array to 1    
        ArrayList<Pair> pair = pairsOfNumber(array);
        
    }   
}

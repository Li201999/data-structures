/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day02twodimarrays;

import java.util.ArrayList;
import java.util.Arrays;
//import junit.framework.Assert;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author faruk
 */
public class Day02TwoDimArraysTest {
    
    /**
     * Test of main method, of class Day02TwoDimArrays.
     */
//    @Test
//    public void testMain() {
//        System.out.println("main");
//        String[] args = null;
//        Day02TwoDimArrays.main(args);
//        
//        // TODO review the generated test code and remove the default call to fail.
//        //fail("The test case is a prototype.");
//    }
    
    @Test
    //method 1 : testsumOfAllNum()
    public void testsumOfAllNum(){
        System.out.println("method sumOfAllNum is testing...");
        int expResult = 63;
        int[][] data2D = {
            {1, 3, 6, 8},
            {7, 1, 2, 3},
            {8, 3, 2, 1},
            {1, 7, 1, 9},
        };
        int result = Day02TwoDimArrays.sumOfAllNum(data2D);
        assertEquals(expResult, result);
    }
    
    @Test
    //method 2 : sumOfEachOfTheRow
    public void sumOfEachOfTheRowTest(){
        System.out.println("method sumOfEachOfTheRow is testing...");
        int[] expResult = {18,13,14,18};
        int[][] data2D = {
            {1, 3, 6, 8},
            {7, 1, 2, 3},
            {8, 3, 2, 1},
            {1, 7, 1, 9},
        };
        int[] result = Day02TwoDimArrays.sumOfEachOfTheRow(data2D);
        //assertEquals(expResult, result);
        assertTrue(Arrays.equals(expResult, result));
    }
    @Test
    //method 3 : sumOfEachOfTheColumn
    public void sumOfEachOfTheColumnTest(){
        System.out.println("method sumOfEachOfTheColumn is testing...");
        int[] expResult = {17,14,11,21};
        int[][] data2D = {
            {1, 3, 6, 8},
            {7, 1, 2, 3},
            {8, 3, 2, 1},
            {1, 7, 1, 9},
        };
        int[] result = Day02TwoDimArrays.sumOfEachOfTheColumn(data2D);
        //assertEquals(expResult, result);
        assertTrue(Arrays.equals(expResult, result));
    }
    
    @Test
    //method 4 : standardDeviation
    public void standardDeviationTest(){
        System.out.println("method standardDeviation is testing...");
        String expResult = "3.05";
        int[][] data2D = {
            {1, 3, 6, 8},
            {7, 1, 2, 3},
            {8, 3, 2, 1},
            {1, 7, 1, 9},
        };
        String result = String.format("%.2f", Day02TwoDimArrays.standardDeviation(data2D));
        //assertEquals(expResult, result);
        assertEquals(expResult, result);
    }
    
    @Test
    //method 5 : pairsOfNumber
    public void pairsOfNumberTest(){
        System.out.println("method pairsOfNumber is testing...");
        String expResult = "[Prime sum   7 of pair value =     1 at [0,0] "
                + "and value =   6 at [0,2], Prime sum   2 of pair value =     "
                + "1 at [0,0] and value =   1 at [1,1], Prime sum   3 of pair "
                + "value =     1 at [0,0] and value =   2 at [1,2], Prime sum   "
                + "3 of pair value =     1 at [0,0] and value =   2 at [2,2], "
                + "Prime sum   2 of pair value =     1 at [0,0] and value =   1 "
                + "at [2,3], Prime sum   2 of pair value =     1 at [0,0] and "
                + "value =   1 at [3,0], Prime sum   2 of pair value =     1 "
                + "at [0,0] and value =   1 at [3,2], Prime sum  11 of pair "
                + "value =     3 at [0,1] and value =   8 at [0,3], Prime sum   "
                + "5 of pair value =     3 at [0,1] and value =   2 at [1,2], "
                + "Prime sum  11 of pair value =     3 at [0,1] and value =   8 "
                + "at [2,0], Prime sum   5 of pair value =     3 at [0,1] and "
                + "value =   2 at [2,2], Prime sum  13 of pair value =     6 "
                + "at [0,2] and value =   7 at [1,0], Prime sum   7 of pair "
                + "value =     6 at [0,2] and value =   1 at [1,1], Prime sum   "
                + "7 of pair value =     6 at [0,2] and value =   1 at [2,3], "
                + "Prime sum   7 of pair value =     6 at [0,2] and value =   1 "
                + "at [3,0], Prime sum  13 of pair value =     6 at [0,2] and "
                + "value =   7 at [3,1], Prime sum   7 of pair value =     6 "
                + "at [0,2] and value =   1 at [3,2], Prime sum  11 of pair "
                + "value =     8 at [0,3] and value =   3 at [1,3], Prime sum  "
                + "11 of pair value =     8 at [0,3] and value =   3 at [2,1], "
                + "Prime sum  17 of pair value =     8 at [0,3] and value =   9 "
                + "at [3,3], Prime sum   3 of pair value =     1 at [1,1] and "
                + "value =   2 at [1,2], Prime sum   3 of pair value =     1 "
                + "at [1,1] and value =   2 at [2,2], Prime sum   2 of pair "
                + "value =     1 at [1,1] and value =   1 at [2,3], Prime sum   "
                + "2 of pair value =     1 at [1,1] and value =   1 at [3,0], "
                + "Prime sum   2 of pair value =     1 at [1,1] and value =   1 "
                + "at [3,2], Prime sum   5 of pair value =     2 at [1,2] and "
                + "value =   3 at [1,3], Prime sum   5 of pair value =     2 "
                + "at [1,2] and value =   3 at [2,1], Prime sum   3 of pair "
                + "value =     2 at [1,2] and value =   1 at [2,3], Prime sum   "
                + "3 of pair value =     2 at [1,2] and value =   1 at [3,0], "
                + "Prime sum   3 of pair value =     2 at [1,2] and value =   1 "
                + "at [3,2], Prime sum  11 of pair value =     2 at [1,2] and "
                + "value =   9 at [3,3], Prime sum  11 of pair value =     3 at "
                + "[1,3] and value =   8 at [2,0], Prime sum   5 of pair value =     "
                + "3 at [1,3] and value =   2 at [2,2], Prime sum  11 of pair "
                + "value =     8 at [2,0] and value =   3 at [2,1], Prime sum  "
                + "17 of pair value =     8 at [2,0] and value =   9 at [3,3], "
                + "Prime sum   5 of pair value =     3 at [2,1] and value =   2 "
                + "at [2,2], Prime sum   3 of pair value =     2 at [2,2] and "
                + "value =   1 at [2,3], Prime sum   3 of pair value =     2 at "
                + "[2,2] and value =   1 at [3,0], Prime sum   3 of pair value =     "
                + "2 at [2,2] and value =   1 at [3,2], Prime sum  11 of pair "
                + "value =     2 at [2,2] and value =   9 at [3,3], Prime sum   "
                + "2 of pair value =     1 at [2,3] and value =   1 at [3,0], "
                + "Prime sum   2 of pair value =     1 at [2,3] and value =   1 "
                + "at [3,2], Prime sum   2 of pair value =     1 at [3,0] and "
                + "value =   1 at [3,2]]";
        int[][] data2D = {
            {1, 3, 6, 8},
            {7, 1, 2, 3},
            {8, 3, 2, 1},
            {1, 7, 1, 9},
        };
        String result = Day02TwoDimArrays.pairsOfNumber(data2D).toString();
        System.out.println("resutl"+result);
        //assertEquals(expResult, result);
        assertEquals(expResult, result);
    }
}
